# Angular Skeleton
## Introduction
This repository is created to provide a Angular application skeleton. You may need to modify files to match your project.

## Installation
First copy `env.dist` file and configure with your custom params and then execute `make` to build and up all dockers container and npm install.

## Other make commands
* `make teardown`: Will call `down` and `remove-network` targets.
* `make create-network`: Will creates required docker network.
* `make remove-network`: Will removes required docker network.
* `make npm-install`: Will connect to node container and run `npm install`.
* `make build`: Will connect to node container and run `ng build --watch`.
* `make build-detached`: Will connect to node container and run `ng build --watch` in background.
* `make unit-test`: Will connect to node container and run `ng test`.
* `make generate type=TYPE name=NAME`: Will connect to node container and run `ng generate`.
* `make up`: Will starts docker containers.
* `make down`: Will stop and remove docker containers.
* `make exec container=CONTAINER_NAME`: Will connect to named docker container's terminal.
* `make node-terminal`: Will uses `exec` to connect to Node docker container terminal.