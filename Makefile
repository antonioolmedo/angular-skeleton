## Setup environment & Install & Build application
setup: create-network up npm-install

## Tear down environment
teardown: down remove-network

## Creates docker network
create-network:
	docker network create nginx_network

## Removes docker network
remove-network:
	docker network rm nginx_network

## Install npm dependencies
npm-install:
	docker exec -w /app angular_skeleton_node_1 npm install

## Execute build and watch in app
build:
	docker exec -w /app angular_skeleton_node_1 ./node_modules/.bin/ng build --watch

## Execute build and watch in app detached
build-detached:
	docker exec -d -w /app angular_skeleton_node_1 ./node_modules/.bin/ng build --watch

## Execute karma test
unit-test:
	 docker exec -w /app angular_skeleton_node_1 npm test

## Generate directive|pipe|service|class|guard|interface|enum|module with name
generate:
	docker exec -w /app angular_skeleton_node_1 ./node_modules/.bin/ng generate ${type} ${name}

## Start docker
up:
	docker-compose up -d

## Stop docker
down:
	docker-compose down

## Go to container terminal
exec:
	docker exec -ti ${container} /bin/bash

## Go to Node container terminal
node-terminal:
	 @$(MAKE) exec container=angular_skeleton_node_1
